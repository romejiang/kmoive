Ext.define('kmoive.view.Moving', {
    extend: 'Ext.Panel',
    xtype: 'moving',
    requires: [],
    config: {
        items: [
            {
                xtype: 'toolbar',
                title: '动态'
            }
        ],
        styleHtmlContent: true,
        html: 'moving'
    }
});
