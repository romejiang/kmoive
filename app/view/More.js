Ext.define('kmoive.view.More', {

    extend: 'Ext.Container',
    xtype: 'more',
    requires:[
        'Ext.TitleBar',
        'Ext.form.FieldSet'
    ],

    config: {
        title: '更多',
        iconCls: 'mymore ',
        layout: {
            type: 'card'
        },

        items: [
            {
                xtype: 'titlebar',
                docked: 'top',
                title: '更多'
            },
            {
                xtype:'panel',
                scrollable:true,
                items: [
                    {
                        xtype: 'fieldset',
                        algin: 'center',
                        items: [
                            {
                                html: '<div class="font1 color1 more_form more_border">' +
                                    '<div style="float: left;margin-right: 10px;margin-top:-2px;"><img src="resources/images/more_1.png"  height="20px" style=" vertical-align: middle"/></div><span>机构信息</span>' +
                                    '</div>',

                                listeners: {
                                    painted: function (label) {
                                        label.on('tap', function () {
                                            //window.localStorage.setItem('MoreDetail_caid', 2);
//                                            Ext.Viewport.animateActiveItem('companyInfor', {type: 'slide', direction: 'left'});
                                            return false;
                                        }, this)
                                    }
                                }
                            },
                            {
                                html: '<div class="font1 color1 more_form more_border">' +
                                    '<span>联系我们</span>' +
                                    '<div style="float: left;margin-right: 10px;margin-top:-2px;"><img src="resources/images/more_2.png"  height="20px" style=" vertical-align: middle"/></div><span style="margin-left: 30px;" id="mynumber">029-88888888</span>' +
                                    '</div>',
                                listeners: {
                                    painted: function (label) {
                                        label.on('tap', function () {
                                            var args = document.getElementById('mynumber').innerHTML
                                            cordova.exec(function (successCallback) {
                                            }, function (errorCallback) {
                                            }, "Redirect", "callNumber", [args]);
                                            return false;
                                        }, this)
                                    }
                                }
                            },
                            {
                                html: '<div class="font1 color1 more_form">' +
                                    '<span>版本更新</span>' +
                                    '<div style="float: left;margin-right: 10px;margin-top:-2px;"><img src="resources/images/more_3.png"  height="20px" style=" vertical-align: middle"/></div><span style="margin-left: 30px;">v1.0</span>' +
                                    '</div>',
                                listeners: {
                                    painted: function (label) {
                                        label.on('tap', function () {
                                            cordova.exec(function(message) { },
                                                function(error) { },
                                                "CheckVersion",
                                                "checkVersion",
                                                ["350962117"]
                                            );
                                            return false;
                                        }, this)
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'fieldset',
                        cls: 'moreFrom',
                        algin: 'center',
                        items: [
                            {
                                id:'collection',
                                html: '<div class="font1 color1 more_form more_border">' +
                                    '<div style="float: left;margin-right: 10px;margin-top:-2px;"><img src="resources/images/more_4.png" height="20px" style=" vertical-align: middle"/></div><span>我的收藏</span>' +
                                    '</div>',
                                listeners: {
                                    painted: function (label) {
                                        label.on('tap', function () {
//                                            Ext.Viewport.animateActiveItem('collection', {type: 'slide', direction: 'left'});
                                            return false;
                                        }, this)
                                    }
                                }
                            },
                            {
                                id:'comments',
                                html: '<div class="font1 color1 more_form more_border">' +
                                    '<div style="float: left;margin-right: 10px;margin-top:-2px;"><img src="resources/images/more_5.png" height="20px" style=" vertical-align: middle"/></div><span>留言反馈</span>' +
                                    '</div>',

                                listeners: {
                                    painted: function (label) {
                                        label.on('tap', function () {
//                                            Ext.Viewport.animateActiveItem('comment', {type: 'slide', direction: 'left'});
                                            return false;
                                        }, this)
                                    }
                                }
                            },
                            {
                                html: '<div class="font1 color1 more_form more_border">' +
                                    '<div style="float: left;margin-right: 10px;margin-top:-2px;"><img src="resources/images/more_6.png" height="20px" style=" vertical-align: middle"/></div><span>消息通知</span>' +
                                    '</div>'
                            },
                            {
                                html: '<div class="font1 color1 more_form more_border">' +
                                    '<div style="float: left;margin-right: 10px;margin-top:-2px;"><img src="resources/images/more_7.png" height="20px" style=" vertical-align: middle"/></div><span>分&nbsp;&nbsp;享&nbsp;&nbsp;至</span>' +
                                    '</div>',

                                listeners: {
                                    painted: function (label) {
                                        label.on('tap', function () {
                                            //分享
                                            cordova.exec(function(winParam) {
                                                },
                                                function(error) {
                                                },
                                                "Share",
                                                "share",
                                                ["‘看电影’客户端太好用啦，快来下载吧 ","",""]
                                            );
                                            return false;
                                        }, this)
                                    }
                                }
                            },
                            {
                                html: '<div class="font1 color1 more_form">' +
                                    '<div style="float: left;margin-right: 10px;margin-top:-2px;"><img src="resources/images/more_8.png" height="20px" style=" vertical-align: middle"/></div><span>一键呼叫</span>' +
                                    '</div>',
                                listeners: {
                                    painted: function (label) {
                                        label.on('tap', function () {
//                                            Ext.Viewport.animateActiveItem('call', {type: 'slide', direction: 'left'});
                                            return false;
                                        }, this)
                                    }
                                }
                            }
                        ]
                    },
                    {
                        xtype: 'fieldset',
                        cls: 'moreFrom',
                        algin: 'center',
                        items: [
                            {
                                html: '<div class="font1 color1 more_form">' +
                                    '<div style="float: left;margin-right: 10px;margin-top:-2px;"><img src="resources/images/more_9.png" height="20px" style=" vertical-align: middle"/></div><span>关于本软件</span>' +
                                    '</div>',

                                listeners: {
                                    painted: function (label) {
                                        label.on('tap', function () {
//                                            window.localStorage.setItem('MoreDetail_caid', 3);
//                                            Ext.Viewport.animateActiveItem('moreDetail', {type: 'slide', direction: 'left'});
                                            return false;
                                        }, this)
                                    }
                                }
                            } 
                        ]
                    }
                ]
            }
        ]
    }
});



