Ext.define('kmoive.view.Coming', {
    extend: 'Ext.Panel',
    xtype: 'coming',
    requires: [],
    config: {
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        scrollable: {
            direction: 'vertical',
            directionLock: true
        },
        items: [
            { xtype: 'label', html: '最受关注'},
            {
                xtype: 'list',
                store: 'Attention',
                id:'attentionList',
                scrollable: false,
                height: '660px',
                itemTpl: "<div style='height:105px; position: relative'>" +
                    "<div style='position: absolute; top:-6px; right: -6px;'>{score}分</div>" +
                    "<img  src='{url}' style='float: left;height: 100px;width: 75px' class='radius'/> " +
                    "<div style='padding-top: 10px;'>{name}</div>" +
                    "<div>{summary}</div>" +
                    "<div>还有{[Math.round((new Date(values.release) - new Date())/1000/60/60/24)]}天上映&nbsp;&nbsp;&nbsp;&nbsp;{attention}人想看</div>" +
                    "</div>" ,
                listeners:{
                    painted:function(){
                        var attention  =Ext.getStore('Attention') ;
                        attention.load();
                    }
                }
            } ,
            { xtype: 'label', html: '即将上映'},
            {
                xtype: 'list',
                store: 'Upcoming',
                id:'upcomingList',
                scrollable: false,
                height: '1180px',
                itemTpl: "<div style='height:105px; position: relative'>" +
                    "<div style='position: absolute; top:-6px; right: -6px;'>{score}分</div>" +
                    "<img  src='{url}' style='float: left;height: 100px;width: 75px' class='radius'/> " +
                    "<div style='padding-top: 10px;'>{name}</div>" +
                    "<div>{summary}</div>" +
                    "<div>还有{[Math.round((new Date(values.release) - new Date())/1000/60/60/24)]}天上映&nbsp;&nbsp;&nbsp;&nbsp;{attention}人想看</div>" +
                    "</div>" ,
                listeners:{
                    painted:function(){
                        var upcoming  =Ext.getStore('Upcoming') ;
                        upcoming.load();
                    }
                }
            }
        ]
    }
});
