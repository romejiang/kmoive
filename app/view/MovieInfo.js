Ext.define('kmoive.view.MovieInfo', {
    extend: 'Ext.Panel',
    xtype: 'movieInfo',
    requires: [],
    config: {
        layout: 'card',
        items: [
            {
                docked:'top',
                xtype: 'toolbar',
                id:'infoTitle',
                items:[
                    {
                        xtype:'button',
                        text:'返回',
                        handler:function(){
                            Ext.Viewport.remove(this.up('movieInfo'),true);
                        }
                    },  {
                        xtype:'button',
                        iconCls:'action',
                        align:'right',
                        action:'newsShare',
                        handler:function(){
                            cordova.exec(function(winParam) { },
                                function(error) {},
                                "Share",
                                "share",
                                ["推荐，快用手机下载安装‘看电影’客户端吧 ",'','']);
                        }

                    }
                ]
            }
        ]
    }
});
