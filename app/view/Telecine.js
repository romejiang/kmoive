Ext.define('kmoive.view.Telecine', {
    extend: 'Ext.Panel',
    xtype: 'telecine',
    requires: [],
    config: {
        layout: "card",
        items:[
            {
                docked: 'top',
                xtype:'toolbar',
                title:'影讯'
            },{
                xtype:'tabpanel',
                cls:'ktop',
                items: [
                    {
                        xtype:'release',
                        title: '正在上映'
                    },
                    {
                        xtype:'coming',
                        title: '即将上映'
                    }
                ]
            }
        ]
    }
});
