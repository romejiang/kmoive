Ext.define('kmoive.view.Cinema', {
    extend: 'Ext.Panel',
    xtype: 'cinema',
    requires: [],
    config: {
        layout:'card',
        items:[
            {
                docked: 'top',
                xtype:'toolbar',
                title:'影院'
            },{
                xtype:'tabpanel',
                cls:"ktop",
                items: [
                    {
                        xtype:'recently',
                        title: '离我最近'
                    },
                    {
                        xtype:'lowest',
                        title: '价格最低'
                    } ,
                    {
                        xtype:'where',
                        title: '所在区域'
                    }
                ]
            }
        ]
    }
});
