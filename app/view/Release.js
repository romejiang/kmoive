Ext.define('kmoive.view.Release', {
    extend: 'Ext.Panel',
    xtype: 'release',
    requires: ['Ext.carousel.Carousel','Ext.Label','Ext.dataview.List'],
    config: {
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        scrollable: {
            direction: 'vertical',
            directionLock: true
        },
        items: [
            {
                xtype: 'carousel',
                direction: "horizontal",
                store: 'DistrictAdPic',
                height: '160px',
                id: 'homeCarousel',
                items: [
                    {
                        xtype: 'panel',
                        items: {
                            html: "<img src='resources/images/1.jpg' style='width:100%;height:100%;'  / >"+
                                '<div style="position:absolute; width:100%; color: #fff; bottom:0;padding-left: 5%; ' +
                                'font-size: 14px; background:#000; opacity: 0.5; filter:alpha(opacity=50);line-height: 24px;">富春山居图</div>'
                        }
                    },
                    {
                        xtype: 'panel',
                        items: {
                            html: "<img src='resources/images/2.jpg' style='width:100%;height:100%;'  / > " +
                            '<div style="position:absolute; width:100%; color: #fff; bottom:0;padding-left: 5%; ' +
                                'font-size: 14px; background:#000; opacity: 0.5; filter:alpha(opacity=50);line-height: 24px;"> 致命黑兰</div>'
                        }
                    } ,
                    {
                        xtype: 'panel',
                        items: {
                            html: "<img src='resources/images/3.jpg' style='width:100%;height:100%;'  / > " +
                                '<div style="position:absolute; width:100%; color: #fff; bottom:0;padding-left: 5%; ' +
                                'font-size: 14px; background:#000; opacity: 0.5; filter:alpha(opacity=50);line-height: 24px;"> 中国合伙人</div>'
                        }
                    } ,
                    {
                        xtype: 'panel',
                        items: {
                            html: "<img src='resources/images/4.jpg' style='width:100%;height:100%;'  / > " +
                                '<div style="position:absolute; width:100%; color: #fff; bottom:0;padding-left: 5%; ' +
                                'font-size: 14px; background:#000; opacity: 0.5; filter:alpha(opacity=50);line-height: 24px;"> 星际迷航</div>'
                        }
                    },
                    {
                        xtype: 'panel',
                        items: {
                            html: "<img src='resources/images/5.jpg' style='width:100%;height:100%;'  / > " +
                                '<div style="position:absolute; width:100%; color: #fff; bottom:0;padding-left: 5%; ' +
                                'font-size: 14px; background:#000; opacity: 0.5; filter:alpha(opacity=50);line-height: 24px;"> 王牌情敌</div>'
                        }
                    },
                    {
                        xtype: 'panel',
                        items: {
                            html: "<img src='resources/images/6.jpg' style='width:100%;height:100%;'  / > " +
                                '<div style="position:absolute; width:100%; color: #fff; bottom:0;padding-left: 5%; ' +
                                'font-size: 14px; background:#000; opacity: 0.5; filter:alpha(opacity=50);line-height: 24px;">  圣诞玫瑰</div>'
                        }
                    }
                ],
                listeners: {
                    initialize: function(carousel){
                        setInterval(function() {
                            if ( carousel.getItems().length> 0) {
                                carousel.next();
                                if (carousel.getActiveIndex() === 5) {
                                    carousel.setActiveItem(0);
                                }
                            }
                        }, 5000);
                    }
                }
            },
            { xtype: 'label', html: '热映中'},
            {
                xtype: 'list',
                store: 'Movie',
                id:'movieList',
                scrollable: false,
                height: '1180px',
                 itemTpl:
                    "<div style='height:105px; position: relative'>" +
                        "<div style='position: absolute; top:-6px; right: -6px;'>{score}分</div>"+
                        "<img  src='{url}' style='float: left;height: 100px;width: 75px' class='radius'/> " +
                        "<div style='padding-top: 10px;'>{name}</div>" +
                        "<div>{summary}</div>"+
                        "<div>今日{stagednum}家影院上映{fieldnum}场</div>"+
                    "</div>"
            }
        ]
    }
});
