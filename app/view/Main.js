Ext.define('kmoive.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar',
        'Ext.Video'
    ],
    config: {
        tabBarPosition: 'bottom',

        items: [
            {
                xtype:'telecine',
                title: '影讯',
                iconCls: 'movie'
            },
            {
                xtype:'cinema',
                title: '影院',
                iconCls: 'cinema'
            },
            {
                xtype:'moving',
                title: '动态',
                iconCls: 'event'
            },
            {
                xtype:'more',
                title: '更多',
                iconCls: 'more'
            }
        ]
    }
});
