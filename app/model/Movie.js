/**
 *
 */
Ext.define('kmoive.model.Movie', {
    extend: 'Ext.data.Model',
    id: 'Movie',
    config: {
        idProperty: 'id',
        fields: ['id',
               'name',
               'url',  //电影的图片url
               'score',//评分
                'scorenum',//评分总人数
               'type',//电影类型
                'release', //上映时间
                'minute',  //定影长度
                'sort',     //所属分类
                'director',  //导演
                'starring',    //主演
                'summary',       // 简介
                'introduce', //详细介绍
                'attention', //多少人关心
                'reviewnum',   //浏览量
                'trailernum',   //预告片数量
                'stillsnum',  //剧照数量
                'stagednum', //上演影院数
                'fieldnum' //上映数量
        ]
    }

});

