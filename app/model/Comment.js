/**
 *
 */
Ext.define('kmoive.model.Comment', {
	extend: 'Ext.data.Model',
	id: 'Comment',
	config: {
		idProperty: 'id',
		fields: ['id',
				'username', //用户名
				'title', // 标题
				'icon', // 头像地址
				'content', // 留言内容
				'image', // 图片地址
				'from', // 信息来源，来自iphone ，来自android
				'up', // 顶的数量
				'movie',  // 电影名称
				'mid', // 电影id
				'lat', // 纬度
				'lng', // 经度
				'recommend', // 是否推荐，精华
				'lasttime'
		]
	}

});