/**
 *
 */
Ext.define('kmoive.store.Comment', {
    extend: 'Ext.data.Store',
    requires: [
        'Ext.data.proxy.JsonP',
        'Ext.data.reader.Json'
    ],
    config: {
        model: 'kmoive.model.Comment',
        id: 'Comment',
        autoLoad: false,
//        proxy: {
//            type: 'jsonp',
//            url: Global.api_url + '/cloud/1/category_all_find',
//            reader: {
//                type: 'json',
//                rootProperty: 'Variables.data'
//            }
//        }
        data: [
//            {'1','富春山居图'}
        ]
    }

});

