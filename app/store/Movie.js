/**
 *
 */
Ext.define('kmoive.store.Movie', {
    extend: 'Ext.data.Store',
    requires: [
        'Ext.data.proxy.JsonP',
        'Ext.data.reader.Json'
    ],
    config: {
        model: 'kmoive.model.Movie',
        id: 'Movie',
        autoLoad: true,
        data: [
            {id:'1',name:'超人：钢铁之躯',url:'resources/images/cr.jpg',score:'7.4',scorenum:'1974',type:'3d',
                release:'2013-06-20',minute:'143',sort:'动作 科幻 冒险',director:'扎克·施奈德',
                starring:'米·亚当斯|凯文·科斯特纳|戴安·琳恩|罗素·克劳',summary:'超人的再次来袭',
                introduce:'克拉克·肯特（Clark Kent/Kal-El ）是一个20来岁的年轻记者，他拥有超出别人想像的能力，因为他是从比地球文明更高级的外星来到地球的。克拉克为“我为什么到这里”而感到困惑纠结，他的地球养父母帮助他形成了人生价值观。很快克拉克就意识到， 拥有超能力就意味着要作出特别艰难的决定——当世界受到威胁、特别需要稳定的时候，他的能力是用来维护和平还是分化和征服呢？克拉克必须成为“超人”，不仅作为这个世界最后的希望灯塔照耀全人类，也要保护他所热爱的一切！',
                attention:'200',reviewnum:'111',trailernum:'55',stillsnum:'20',stagednum:'14',fieldnum:'51'} ,
            {id:'2',name:'不二神探',url:'resources/images/br.jpg',score:'5.7',scorenum:'6544',type:'2d',
                release:'2013-06-21',minute:'98',sort:'动作 喜剧 犯罪',director:'王子鸣',
                starring:'李连杰|文章|刘诗诗|陈妍希|柳岩|吴京|梁家仁',summary:'李连杰“毒舌”文章喜感',
                introduce:'数日内，三起“微笑杀人案”震动全城。调查过程中，青年警探、警局“活宝”王不二语出惊人：这是一起连环谋杀案！遂与搭档黄非红开始了一段惊悚刺激，同时又状况不断的“缉凶”之旅。黄非红在旁边看似总是乌龙，但其实是真正的功夫高手，每到关键时刻，他总能帮助王不二化险为夷。案件侦办过程中，王不二先是将怀疑对像锁定为女明星刘金水，随着案情的深入，刘金水的嫌疑被逐渐撇清，她的姐姐戴依依等人又成了王不二的...',
                attention:'300',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'12',fieldnum:'21'},
            {id:'3',name:'富春山居图',url:'resources/images/fcs.jpg',score:'3.4',scorenum:'32404',type:'3d',
                release:'2013-06-09',minute:'122',sort:'动作 冒险',director:'孙健君',
                starring:'刘德华|林志玲|张静初|佟大为|斯琴高娃',summary:'李连杰“毒舌”文章喜感',
                introduce:'中国元代名画《富春山居图》合璧展在即，国际黑市开出天价，日本黑帮、英伦大盗闻风而动。身陷不白之冤的国际特工肖锦汉（刘德华 饰）为证清白重出江湖，暗中执行“孙子兵法”计划；临危受命的中国人保高管林雨嫣（张静初 饰），护宝遇一波三折，困于多方势力漩涡；神秘女郎丽莎（林志玲 饰），百变魅惑，却失落真心；幕后冷血黑手小山本（佟大为 饰），嗜血如命，终落败归尘；但各怀绝技的12星座魔女（石天琦、王曼妮 等饰）为小山本作奸犯科，忠心到极致。肖锦汉为夺画上天入地钻沙漠，能否力克强敌，重现昔日特工风采？林雨嫣博弈于西子湖畔，能否与肖锦汉重归旧好？丽莎险些命丧迪拜，能否摆脱魔爪，迎来涅槃？这一场生死对决，这一段恩怨情仇，都随《富春山居图》的安然归国，迎来命定结局。',
                attention:'302',reviewnum:'221',trailernum:'52',stillsnum:'31',stagednum:'13',fieldnum:'31'},
            {id:'4',name:'北漂鱼',url:'resources/images/bpy.jpg',score:'4.8',scorenum:'100',type:'2d',
                release:'2013-06-21',minute:'88',sort:'喜剧',director:'余治林',
                starring:'姜峰|林子聪|尼玛·颂宋|雷茜|余治林',summary:'致我们终将逝去的鱼缸',
                introduce:'《北漂鱼》讲述的是一个为了追逐电影梦想的大龄北漂余小宝隐瞒了自己拮据苦闷的生活现状，在老家的女儿小妮面前将自己描述成了成功的大导演。小妮的突然到来打乱了余小宝的生活。为了圆谎，小宝向各路北漂兄弟姐妹寻求帮助，而事情远没有想象得那么容易，北漂的苦辣酸甜、一幕幕的百态人生也就此登场。',
                attention:'300',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'14',fieldnum:'51'},
            {id:'5',name:'星际迷航：暗黑无界',url:'resources/images/ahwj.jpg',score:'8.1',scorenum:'8303',type:'3d',
                release:'2013-05-28',minute:'132',sort:'动作 科幻 冒险',director:'J·J·艾布拉姆斯',
                starring:'克里斯·派恩|扎克瑞·昆图|布鲁斯·格林伍德',summary:'星际迷航精彩续集',
                introduce:'艾伯拉姆斯透露了“企业号”下一步行动的信息：“作为《星际迷航》的一部续集，其野心并不仅是要成为一部让观众值得一看的电影，也不仅仅是另一部电影，而是应该让人们明白，《星际迷航2》中会加入了更多新的东西。在第一部里，我们更多的是在关注角色的塑造，让各个人物得以相见，并讲述一些家族故事。而作为它的续集，从很多方面来说，都要与过去有很多的不同。”“续集里应该有与第一集似曾相识和相互关联的情节，还要有恢宏壮观的视觉效果，当然了，前提是它必须是一个引人入胜的故事，我们希望它能够打动人心。”艾伯拉姆斯说。至于如何在这部科幻题材影片中加入现实世界的元素，正如导演所透露有可能涉及的全球时政问题，尤其是当前紧张的战争局势，则引发了影迷和媒体的大猜想。《洛杉矶时报》的记者Geoff Boucher推测，《星际迷航2》电影中，冲突有可能会在星际舰队和克林贡学院之间爆发。至于最终结果怎样，就让我们拭目以待。该片目前锁定的上映时间为2011年的暑期档。',
                attention:'300',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'15',fieldnum:'51'},
            {id:'6',name:'中国合伙人',url:'resources/images/zghhr.jpg',score:'7.9',scorenum:'6244',type:'2d',
                release:'2013-05-17',minute:'112',sort:'剧情',director:'陈可辛',
                starring:'黄晓明|邓超|佟大为|杜鹃',summary:'致他们终将牛B的青春',
                introduce:'该片讲述了“土鳖”黄晓明、“海龟”邓超、“愤青”佟大为从1980年代到21世纪，30年的大变革背景下，三兄弟为了改变自身命运，创办英语培训学校，最终实现“中国式梦想”的“屌丝逆袭故事”。电影讲述结识于80年代的三位大学好友成东青、孟晓骏、王阳，一起打架，泡妞，梦想去美国。在时代激流中，有的美国梦圆，有的美国梦碎，有的美国梦圆了又碎——惟齐力创办英语培训学校的新梦想，将三子紧紧绑在了一起。',
                attention:'300',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'10',fieldnum:'11'},
            {id:'7',name:'致命黑兰',url:'resources/images/zmhl.jpg',score:'7.0',scorenum:'6544',type:'2d',
                release:'2013-06-07',minute:'108',sort:'动作 剧情 惊悚',director:'奥利维尔·米加顿',
                starring:'佐伊·索尔达娜|迈克尔·瓦尔坦|马克斯·马蒂尼',summary:'充满危险的复仇路',
                introduce:'CataleyaRestrepo（佐伊·索尔达娜饰）是个出生在哥伦比亚的女孩，她在10岁时亲历了自己的父母被哥伦比亚毒枭唐·路易斯杀害，侥幸逃脱的她前往美国芝加哥投奔在那里谋生的舅舅EmilioRestrepo（克利夫·柯蒂斯饰）。虽然他也是一个黑帮团伙的成员，但他希望Cataleya能远离黑帮势力，不要步自己的后尘，更不希望她去复仇，只愿她能平平静静地生活。但Cataleya复仇心切，一心想成为一名能为父母报仇的职业杀手。如今25岁的Cataleya已成为一名技艺高超、经验丰富的杀手，她平时在为舅舅工作，但心中却在不断酝酿着自己的复仇计划，当舅舅Emilio全家遇害后，Cataleya决定开始将自己的得分计划一步步付诸实施...',
                attention:'300',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'12',fieldnum:'21'},
            {id:'8',name:'逆光飞翔',url:'resources/images/ngfx.jpg',score:'8.3',scorenum:'369',type:'2d',
                release:'2013-06-08',minute:'107',sort:'剧情 爱情',director:'张荣吉',
                starring:'张榕容|黃裕翔|李烈|许芳宜|柯淑勤|纳豆|黄采仪',summary:'为了梦想而前行',
                introduce:'天生眼盲的裕翔，首次離家北上唸書，他琴彈得好，卻堅持不參加任何比賽，因為不想被同情。愛跳舞的小潔被迫放棄學舞，卻交了個一跳街舞就閃耀著光芒的男友。兩顆平行的心，竟在不經意間望見彼此身後那道逆光，等待展翅飛翔的可能。曾以《天黑》在台北電影節獲獎的張榮吉執導的首部劇情長片，影像細膩寫實，讓張榕容的內斂與黃裕翔的本色演出，不可思議地揉合成一頁溫柔詩篇，更是台片年度驚喜。',
                attention:'300',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'13',fieldnum:'31'},
            {id:'9',name:'潜艇总动员3：彩虹宝藏',url:'resources/images/chbz.jpg',score:'4.7',scorenum:'6544',type:'2d',
                release:'2013-05-31',minute:'80',sort:'动画 儿童',director:'何子力',
                starring:'范楚绒|洪海天|谢元真|胡谦|刘北辰|严丽祯|谭满堂',summary:'向彩虹宝藏出发！',
                introduce:'大家知道彩虹是怎样产生的吗？传说，每当暴风雨过后，阳光再次洒向大地，洒在世界尽头那堆积如山的珠宝上，折射出五彩斑斓的七色光芒，射向天空，就形成了彩虹。 <br>这次我们的小潜艇阿力、贝贝和他们的小伙伴波波，还有海马家族又出发了。他们将进行一次奇妙、惊险、刺激的寻宝之旅，向着世界尽头，那传说中的彩虹宝藏出发！ <br>贪婪的独眼鲨当然也想得到宝藏。他密谋要利用小潜艇们带他到达彩虹岛，然后独占宝藏。 <br>寻宝的队伍在兴奋和期待中顺利启航。一路虽然风光无限，但狂风暴雨和危险时而相伴，更有独眼鲨阴谋一步步向他们逼进。有人想放弃，有人在坚持；有人默默努力，有人伺机破坏。 <br>历经千辛，小伙伴们终于找到了宝藏，并且战胜了贪婪的独眼鲨。但是，此刻大家明白到，得到的宝藏换不来欢笑，更让他们明白贪婪财富还会带来灾难；而友谊、亲情就像彩虹一样绚丽多彩、耀眼夺目，这才是我们所需要的。 ',
                attention:'300',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'8',fieldnum:'14'}
        ]
    }

});

