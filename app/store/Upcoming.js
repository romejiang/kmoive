/**
 *
 */
Ext.define('kmoive.store.Upcoming', {
    extend: 'Ext.data.Store',
    requires: [
        'Ext.data.proxy.JsonP',
        'Ext.data.reader.Json'
    ],
    config: {
        model: 'kmoive.model.Movie',
        id: 'Upcoming',
        autoLoad: true,
//        proxy: {
//            type: 'jsonp',
//            url: Global.api_url + '/cloud/1/category_all_find',
//            reader: {
//                type: 'json',
//                rootProperty: 'Variables.data'
//            }
//        }
        data: [
            {id:'1',name:'独行侠',url:'resources/images/s24944289.jpg',score:'7.4',scorenum:'1974',type:'3d',
                release:'2013-07-01',minute:'143',sort:'动作 冒险',director:'戈尔·维宾斯基',
                starring:'约翰尼·德普 / 艾米·汉莫 / 汤姆·威尔金森 / 海伦娜·邦汉·卡特 ',summary:'面具游侠西部旷野惩恶扬善',
                introduce:'曾执导过《哈利波特与火焰杯》（Harry Potter and the Goblet of Fire）的英国导演迈克·纽维尔（Mike Newell）正与迪士尼洽谈，准备执导约翰尼·德普（Johnny Depp）主演的西部片《游侠传奇》（The Lone Ranger）。 《游侠传奇》最早是一出电台节目，诞生于30年代，之后被改编成电影、电视、漫画、小说等多种形式。《游侠传奇》的主人公是一名戴着面具的游侠（类似于佐罗），他原本是德州骑警，在追捕一伙不法之徒时险些送命，在印第安人Tonto（约翰尼·德普）的治疗和照顾下逐渐康复，并从此戴上面具、骑着白马和Tonto一起在西部旷野惩恶扬善。 迪士尼的新版《游侠传奇》由Ted Elliot 和 Terry Rossio编剧。迈克·纽维尔目前正为迪士尼制作《波斯王子：时间之沙》（Prince of Persia: The Sands of Time）。',
                attention:'924',reviewnum:'111',trailernum:'55',stillsnum:'20',stagednum:'14',fieldnum:'51'} ,
            {id:'2',name:'怪兽大学',url:'resources/images/p1896375401.jpg',score:'5.7',scorenum:'6544',type:'2d',
                release:'2013-07-01',minute:'98',sort:'动作 喜剧 犯罪',director:'丹·斯坎伦',
                starring:'约翰·古德曼 / 比利·克里斯托 / 史蒂夫·布西密 / 海伦·米伦',summary:'珍贵友情带来的欢乐与感动',
                introduce:'大眼仔迈克（Mike Wazowski）和毛怪萨利（James P. Sullivan）是最好的朋友，不过他们可不是一开始就是这样的。想当年他们俩在怪兽大学第一次见面的时候，双方可是谁都无法忍受谁的，那么他们是怎样抛弃成见、求同存异、成为朋友的呢？这就是《怪兽大学》要讲述的故事。 约翰·古德曼（John Goodman）与比利·克里斯托（Billy Crystal）将继续为毛怪萨利（Sully）和大眼仔迈克（Mike）配音，而观众在看到很多老面孔的同时也将会遇到一群新的怪兽。',
                attention:'1364',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'12',fieldnum:'21'},
            {id:'3',name:' 秦时明月',url:'resources/images/p1994460614.jpg',score:'3.4',scorenum:'32404',type:'3d',
                release:'2013-07-01',minute:'122',sort:'动画 奇幻 冒险 武侠',director:'沈乐平',
                starring:'冯骏骅 / 刘钦 / 沈磊 / 沈乐平',summary:'楼兰古国的异域风情',
                introduce:'改编自拥有500万粉丝的中国第一人气武侠动画系列剧《秦时明月》，即将于2013年暑期上映！ 故事讲述了少年天明，少羽在机缘巧合之下，一起卷入了秦始皇夺取远古龙魂的阴谋行动之中，传说中的仙境即将再次陷入千年未遇的劫难！ 来自茫茫沙海的陌生少女，美丽而又神秘，和她的相遇仿佛是命运的指引，跨越千年的时空，冥冥之中早已注定 秦军战旗环伺之下，来自上古的可怕力量正在苏醒！面对这场浩劫，他们能否成功扭转乾坤，拯救天下苍生？ 这个关于爱与希望的动人故事，且听《秦时明月大电影龙腾万里》为你娓娓道来！',
                attention:'1309',reviewnum:'221',trailernum:'52',stillsnum:'31',stagednum:'13',fieldnum:'31'},
            {id:'4',name:'生死迷局 M',url:'resources/images/p1858039822.jpg',score:'4.8',scorenum:'100',type:'2d',
                release:'2013-07-01',minute:'88',sort:'惊悚',director:'埃里克·罗查特',
                starring:'让·杜雅尔丹 / 西西·迪·法兰丝 / 蒂姆·罗斯 / 埃里克·罗查',summary:'离奇命案引发的连串迷局',
                introduce:'《莫比乌斯》主要讲述了让·杜雅尔丹饰演的联邦安全局长官调查一位俄罗寡头政治者的洗钱黑幕。',
                attention:'300',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'14',fieldnum:'51'},
            {id:'5',name:'蝙蝠别墅',url:'resources/images/p1965258461.jpg',score:'8.1',scorenum:'8303',type:'3d',
                release:'2013-07-01',minute:'132',sort:'动作 科幻 冒险',director:'黄明升',
                starring:'武艺 / 邓紫衣 / 段黄巍 / 贺贤威 / 罗家英 / 李亮文',summary:'俊男靓女惨遭血腥袭击',
                introduce:'一场热闹的摇滚现场演出正在上演。台上热力表演的是正当红的流行乐队。由于迟迟没有创作出新作品，乐队为了应对被解散的危机，来到郊区的一幢别墅里封闭排练和创作。别墅屋顶，密密麻麻吊挂着一大片蝙蝠，乐队戏称蝙蝠别墅。 乐队在新环境灵感迸发排练新歌，伴随吉他、贝斯声音在静谧的山间响起，夜幕下的别墅惊现一堆血淋淋的蝙蝠尸体，原本不伤人的蝙蝠开始嗜血攻击，乐队成员也被若有似无的女人哭泣声刺激着心绪。随着命案的接连发生，乐队成员开始注意到，在蝙蝠别墅内，除了蝙蝠伤人之外，还有更惊悚的事件在等待着他们。',
                attention:'154',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'15',fieldnum:'51'},
            {id:'6',name:'飘落的羽毛',url:'resources/images/p1992499683.jpg',score:'7.9',scorenum:'6244',type:'2d',
                release:'2013-07-01',minute:'112',sort:'剧情',director:'王艺',
                starring:'郑晓东 / 刘筱筱 / 屈菁菁 / 董可飞 / 刘洋影子 / 王艺',summary:'一段凄美的爱情故事',
                introduce:'著名画家莫克宣布自己的油画作品飘落的羽毛所拍得全画款捐给了西南边陲小镇蒙自，没人知道缘由，他只身来到久别的小镇蒙自 二十多年前，年青莫克路过蒙自，被这里的景致迷住了，决定在此写生。 莫克，生于1949年，国际著名的华裔色彩主义抽象派色彩主义画家，2008年奥林匹克美术大会金奖得主，第18界国际美学大会艺术人物，2012年伦敦奥林匹克美术大会艺术指导委员会副主席，经济学硕士、动物学学士，世界级艺术家，常年居住在中国，作品多受宗教影响，绘画具有佛教人文浪漫主义色彩。',
                attention:'300',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'10',fieldnum:'11'},
            {id:'7',name:'我爱的是你爱我',url:'resources/images/s24586865.jpg',score:'7.0',scorenum:'6544',type:'2d',
                release:'2013-07-01',minute:'108',sort:'剧情 爱情 悬疑',director:'马志宇',
                starring:'张涵予 / 王珞丹 / 王柏杰 / 朱丹 / 孙兴 / 李呈媛 / 张浩天',summary:'王珞丹为角色苦练钢管舞',
                introduce:'亚鹏37，沈小乐19，大叔萝莉谈起了恋爱。时间齿轮旋转，分手宛如打开了潘多拉魔盒。爱恨、困惑、纠结、他们的真爱到底经历怎样的曲折，他说：我以为我爱的是你爱我；她说：我爱你，爱是大于一切的！',
                attention:'300',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'12',fieldnum:'21'},
            {id:'8',name:'盲探',url:'resources/images/p2003777772.jpg',score:'8.3',scorenum:'369',type:'2d',
                release:'2013-07-04',minute:'107',sort:'喜剧 爱情 悬疑',director:'杜琪峰',
                starring:'刘德华 / 郑秀文 / 郭涛 / 姚莹莹 / 卢海鹏 / 朱咪咪 / 李成昌 ',summary:'为了梦想而前行',
                introduce:'天生眼盲的裕翔，首次離家北上唸書，他琴彈得好，卻堅持不參加任何比賽，因為不想被同情。愛跳舞的小潔被迫放棄學舞，卻交了個一跳街舞就閃耀著光芒的男友。兩顆平行的心，竟在不經意間望見彼此身後那道逆光，等待展翅飛翔的可能。曾以《天黑》在台北電影節獲獎的張榮吉執導的首部劇情長片，影像細膩寫實，讓張榕容的內斂與黃裕翔的本色演出，不可思議地揉合成一頁溫柔詩篇，更是台片年度驚喜。',
                attention:'1848 ',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'13',fieldnum:'31'},
            {id:'9',name:'赤焰战场2',url:'resources/images/p1971523187.jpg',score:'4.7',scorenum:'6544',type:'2d',
                release:'2013-08-01',minute:'80',sort:'喜剧 动作 惊悚 犯罪',director:'迪恩·帕里索特',
                starring:'布鲁斯·威利斯 / 约翰·马尔科维奇 / 安东尼·霍普金斯 / 李秉',summary:'双枪“女王”火力全开',
                introduce:'全美票房近亿的大片《赤焰战场》的续集全面升级登场，火力更猛！退而不休的几位前CIA干员，凭着犀利依旧的身手，将再度重出江湖，迎接超乎想像的全新任务！布鲁斯·威利斯等大牌明星超强组合。',
                attention:'800',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'8',fieldnum:'14'}
        ]
    }

});

