/**
 *
 */
Ext.define('kmoive.store.Attention', {
    extend: 'Ext.data.Store',
    requires: [
        'Ext.data.proxy.JsonP',
        'Ext.data.reader.Json'
    ],
    config: {
        model: 'kmoive.model.Movie',
        id: 'Attention',
        autoLoad: false,
        data: [
            {id:'1',name:'小时代',url:'resources/images/p2004121607.jpg',score:'1.7',scorenum:'1974',type:'2d',
                release:'2013-06-27',minute:'143',sort:'爱情',director:'郭敬明',
                starring:'杨幂|柯震东|郭采洁|郭碧婷|谢依霖|凤小岳|陈学冬 ',summary:'杨幂郭采洁献激吻',
                introduce:'这是一个梦想闪耀的时代，这也是一个理想冷却的时代；这是最坏的时代，这也是最好的时代，这是我们的小时代。 这是当下时代一群时尚年轻人的青春故事，也是属于他们生活的真实写照，更是我们这个时代的一个缩影。 故事发生在经济飞速发展时期的上海，主人公林萧（杨幂 饰）、南湘（郭碧婷 饰）、顾里（郭采洁 饰）、唐宛（谢依霖 饰）如四个女生在这座风光而时尚的城市里生活与学习、工作与成长，四个女生从小感情深厚，却各自有着不同的价值观与人生观，她们在同一个宿舍朝夕相处，转眼到了大学生涯的后期，平静的生活开始面临层出不穷的挑战，找工作实习的忙碌以及随之而来的巨大生存压力，看似平静的校园生活相继发生着种种让她们措手不及、不知如何面对、需要抉择的事情。同时，顾源（柯震东 饰）、宫洛（凤小岳 饰）、简溪（李悦铭 饰）、崇光（陈学冬 饰）等一群男生和这四个女生之间也正发生着千丝万缕的情感交错 面临巨大生存压力，面对剪不断理还乱的情感纠葛，四个女生要先后经历友情、爱情，乃至亲情的巨大转变，在巨变面前，她们是否依然坚持自己的生活态度，她们的青春故事将如何续写精彩？ 本片根据同名畅销小说《小时代》改编，郭敬明亲自操刀编剧，并首度跨界担任电影导演。',
                attention:'8989',reviewnum:'111',trailernum:'55',stillsnum:'20',stagednum:'14',fieldnum:'51'} ,
            {id:'2',name:'萨米大冒险2',url:'resources/images/p2009535643.jpg',score:'5.7',scorenum:'6544',type:'2d',
                release:'2013-06-28',minute:'98',sort:'动画 冒险',director:'本·斯塔森',
                starring:'帕特·卡洛尔|卡洛斯·麦卡勒斯二世|信达·亚当斯|迪诺·安',summary:'奇小龟历险记',
                introduce:'上回的主角海龟森美和朋友阿威已升级做爷爷，凑住两个乖孙：里奇同恩娜出海。谁不知未出海就被人类捉住上船，并遇上有精神分裂的龙虾劳苏和扮死高手怪鱼尖波，原来他们正要被送往全球最庞大最奢华的高科技人造水世界做展品，森美和阿威更在途中同两个乖孙失散！ 被困在海底监仓的森美和阿威，认识到各种外型趣怪性格可爱的海洋朋友，有烂gag小丑鱼杜斯、扁头鲨鱼阿拔、神经质鱼积斯、痴缠鱼夫妇猛南同索侣，大家各出奇谋，誓要重拾自由。无奈海马老大大Dee，硬要当家做主诸多阻挠，大家被迫跟从他很不实际的逃亡大计。与此同时，里奇和恩娜在大海中四处寻找爷爷，结识了八爪鱼母女玛嘉烈同小安妮。森美和阿威忽然想出一条妙招，召集一班海洋朋友同里奇、恩娜与八爪鱼救兵里应外合，实行越狱大逃亡。',
                attention:'8222',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'12',fieldnum:'21'},
            {id:'3',name:'开心超人',url:'resources/images/p1942353641.jpg',score:'3.4',scorenum:'32404',type:'2d',
                release:'2013-06-28',minute:'122',sort:'动作 冒险',director:'黄伟明',
                starring:'邓玉婷|祖晴|高全胜|刘红韵|严彦子|陈明|金铭 ',summary:'小超人奋勇斗争大小怪邪恶势力',
                introduce:'电影《开心超人》主要讲述在神奇的星星球上，开心超人、甜心超人、花心超人、粗心超人、小心超人五个可爱的小超人为保卫家园团结一心，与大大怪将军和小小怪下士代表的邪恶力量奋勇斗争，挫败敌人阴谋，最终赢得胜利的故事。',
                attention:'8111',reviewnum:'221',trailernum:'52',stillsnum:'31',stagednum:'13',fieldnum:'31'},
            {id:'4',name:'赤警威龙',url:'resources/images/p1924228661.jpg',score:'4.8',scorenum:'100',type:'2d',
                release:'2013-07-01',minute:'88',sort:'动作 惊悚 犯罪',director:'沃尔特·希尔',
                starring:'西尔维斯特·史泰龙|姜成镐|莎拉·沙黑|阿德沃尔·阿吉纽',summary:'史泰龙利斧肉搏场面惊人',
                introduce:'史泰龙扮演一个纽约刺客，影片讲述了他与一个新奥尔良警察合作，面对共同的敌人，并揭开了一个不为人知的阴谋。',
                attention:'8100',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'14',fieldnum:'51'},
            {id:'5',name:'穿越火线',url:'resources/images/p1988650099.jpg',score:'8.1',scorenum:'8303',type:'3d',
                release:'2013-07-01',minute:'132',sort:'动作 科幻 冒险',director:'贾尼克·法伊济耶夫',
                starring:'斯维塔兰娜·伊万诺娃|弗拉季米尔·弗多维琴科夫|尤里·库',summary:'一场战争的两个视角',
                introduce:'影片故事讲述一位离异的母亲为了和新男友共度周末，不得不将宝贝儿子交给当兵的前夫代管。谁知，前夫执行任务的地方却爆发激烈战事，令儿子也身陷战区、命运未卜。母亲于是只身前往，要救出孩子脱离险境',
                attention:'6374',reviewnum:'121',trailernum:'45',stillsnum:'21',stagednum:'15',fieldnum:'51'}
        ]
    }

});

