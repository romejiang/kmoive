Ext.define('kmoive.controller.MovieInfo', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            release: 'release',
            coming: 'coming',
            movieList: 'release #movieList',
            attentionList: 'coming #attentionList',
            upcomingList: 'coming #upcomingList'
        },
        control: {
            product_select: {
                itemtap: function (list, index, element, record){
                    if (window.localStorage.getItem("productClick") == null) {
                        Ext.Viewport.mask({xtype: 'loadmask' });
                        window.localStorage.setItem("productClick", '1');
                        localStorage.setItem('cpid', record.getData().cpid);
                        localStorage.setItem('isHome', 1);
                        var product_detail = this.getDetail();
                        if (product_detail == null) {
                            product_detail = Ext.create('qjtravel.view.ProductDetail');
                        }
                        var label = product_detail.query('label');
                        Ext.each(label, function (elem) {
                            elem.setData(record.data);
                        });
                        record.data.tel == ' ' ? Ext.getCmp('tel').hide() : product_detail.query('#tel')[0].setHtml(
                            '<div style="float: left;margin-right: 10px;"><img src="resources/images/tel2.png" height="20px" style=" vertical-align: middle"/></div>'+record.data.tel);
                        record.data.address == ' ' ? Ext.getCmp('address').hide() : product_detail.query('#address')[0].setHtml(
                            '<div style="float: left;margin-right: 10px;"><img src="resources/images/postion.png" height="20px" style=" vertical-align: middle"/></div>'+record.data.address);
                        if (record.data.address == ' ' && record.data.tel == ' ') {
                            Ext.getCmp('fieldset').hide()
                        }

                        product_detail.query('#name')[0].setValue(record.data.name);
                        product_detail.query('#picurl')[0].setValue(record.data.picurl);
                        product_detail.query('#explain')[0].setValue(record.data.explain);
                        Ext.getCmp('toolbars').setTitle(record.data.name);
                        Ext.getCmp('callNumber').setValue(record.data.tel);
                        Ext.Viewport.animateActiveItem(product_detail, {type: 'slide', direction: 'left'});
                    }
                }
            }
        }
    }
});